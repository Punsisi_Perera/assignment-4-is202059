#include <stdio.h>

int main(void) {

float height,radius,vol;

printf("Enter the height of the cone: ");
scanf("%f",&height);

printf("Enter the radius of the cone: ");
scanf("%f",&radius);

vol = 3.14*radius*radius*height/3;

printf("The volume of the cone is = %.2f \n",vol);

return 0;
}